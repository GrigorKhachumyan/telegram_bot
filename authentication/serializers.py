from rest_framework import serializers

from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    confirm_password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'password', 'first_name', 'last_name', 'confirm_password']
        extra_kwargs = {
            'password': {'write_only': True},
        }

    def validate_password(self, value):
        password = value
        if len(password) < 8:
            raise serializers.ValidationError("Password is too short")
        return password

    def validate(self, data):
        confirm_password = data['confirm_password']
        password = data['password']
        if confirm_password != password:
            raise serializers.ValidationError("confirmed password was wrong")
        return data

    def create(self, validated_data):
        validated_data.pop('confirm_password', None)
        user = User.objects.create_user(username=validated_data.get('username'),
                                        email=validated_data.get('email', ''),
                                        password=validated_data.get('password'),
                                        first_name=validated_data.get('first_name', ''),
                                        last_name=validated_data.get('last_name', ''))
        return user

