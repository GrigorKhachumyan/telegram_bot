from authentication.serializers import UserSerializer
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from django.contrib.auth.models import User


class RegisterUserView(generics.CreateAPIView):
    serializer_class = UserSerializer
    permission_classes = ()


class UserProfileView(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_object(self):
        queryset = self.get_queryset()
        userObj = get_object_or_404(queryset, id=self.request.user.id)
        return userObj
