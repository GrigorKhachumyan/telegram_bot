from django.urls import path, include
from authentication.views import UserProfileView, RegisterUserView
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token


urlpatterns = [
    path('login/', obtain_jwt_token),
    path('token_refresh/', refresh_jwt_token),
    path('user_profile/', UserProfileView.as_view(), name='user-profile'),
    path('signup/', RegisterUserView.as_view(), name='create-user'),
]
