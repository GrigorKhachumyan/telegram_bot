from django.urls import path
from main.views import *

urlpatterns = [
    path('organizer/<str:pk>', OrganizerAPIView.as_view(), name='organizer'),
    path('organizer', OrganizerAPIView.as_view(), name='organizer'),

    path('organizer_employee/<str:pk>', OrganizerEmployeeAPIView.as_view(), name='organizerAgent'),
    path('organizer_employee', OrganizerEmployeeAPIView.as_view(), name='organizerAgent'),

    path('application/<int:pk>', ApplicationView.as_view(), name='application'),
    path('application/', ApplicationView.as_view(), name='application'),

    path('application_status/', ApplicationStatusView.as_view(), name='applicationStatus'),
]
