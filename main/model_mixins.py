from django.db import models


class ApplicationBaseMixin(models.Model):
    # direction = models.ForeignKey('DeliveryDirection', models.CASCADE, related_name="app_delivery_direction")
    # length = models.PositiveIntegerField()
    # width = models.PositiveIntegerField()
    # height = models.PositiveIntegerField()
    delivery_date = models.DateTimeField()
    created_date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey('OrganizerEmployee', models.CASCADE)
    status = models.ForeignKey('OrganizerStatusSets', on_delete=models.CASCADE, verbose_name='статус заявки')

    class Meta:
        abstract=True
