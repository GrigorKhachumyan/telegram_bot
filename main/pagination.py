from rest_framework.pagination import CursorPagination


class CursorPagination(CursorPagination):
    page_size = 4
    cursor_query_param = 'CP'
    ordering = 'id'
