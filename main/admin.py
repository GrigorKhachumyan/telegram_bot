from django.contrib import admin
from main.models import (
    Organizer,
    OrganizerEmployee,
    Executor,
    ExecutorAgent,
    Application,
    DeliveryDirection,
    NativeExecutor,
    ApplicationExecutorView,
    ApplicationOffer,
    ApplicationComment,
    ApplicationChangeHistory,
    OrganizerStatusSets
)


class OrganizerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
    ordering = ('id',)

    class Meta:
        model = Organizer


class ExecutorAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'inn')
    ordering = ('id',)

    class Meta:
        model = Executor


class NativeExecutorAdmin(admin.ModelAdmin):
    list_display = ('id', 'host_company', 'fixed_price_per_km', 'contract', 'executor',)
    ordering = ('id',)

    class Meta:
        model = NativeExecutor


class OrganizerAgentAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'company',)
    ordering = ('id',)

    class Meta:
        model = OrganizerEmployee


class ExecutorAgentAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'surname', 'executor',)
    ordering = ('id',)

    class Meta:
        model = ExecutorAgent


class ApplicationAdmin(admin.ModelAdmin):
    list_display = ('id', 'author', 'is_being_edited', 'editor', 'status', 'delivery_date', 'created_date',)
    ordering = ('id',)

    class Meta:
        model = Application


class ApplicationChangeHistoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'application',)
    ordering = ('id',)

    class Meta:
        model = ApplicationChangeHistory


class DeliveryDirectionAdmin(admin.ModelAdmin):
    list_display = ('id', 'country', 'city', 'address',)
    ordering = ('id',)

    class Meta:
        model = DeliveryDirection


class ApplicationExecutorViewAdmin(admin.ModelAdmin):
    list_display = ('id', 'executor', 'application', 'seen_date',)
    ordering = ('id',)

    class Meta:
        model = ApplicationExecutorView


class ApplicationOfferAdmin(admin.ModelAdmin):
    list_display = ('id', 'executor', 'application', 'offer_date', 'price',)
    ordering = ('id',)

    class Meta:
        model = ApplicationOffer


class ApplicationCommentAdmin(admin.ModelAdmin):
    list_display = ('id', 'executor', 'application', 'comment_date', 'comment',)
    ordering = ('id',)

    class Meta:
        model = ApplicationComment


class OrganizerStatusSetsAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'company',)
    ordering = ('id',)

    class Meta:
        model = OrganizerStatusSets


admin.site.register(Organizer, OrganizerAdmin)
admin.site.register(OrganizerEmployee, OrganizerAgentAdmin)
admin.site.register(Executor, ExecutorAdmin)
admin.site.register(ExecutorAgent, ExecutorAgentAdmin)
admin.site.register(Application, ApplicationAdmin)
admin.site.register(DeliveryDirection, DeliveryDirectionAdmin)
admin.site.register(NativeExecutor, NativeExecutorAdmin)
admin.site.register(ApplicationOffer, ApplicationOfferAdmin)
admin.site.register(ApplicationExecutorView, ApplicationExecutorViewAdmin)
admin.site.register(ApplicationComment, ApplicationCommentAdmin)
admin.site.register(ApplicationChangeHistory, ApplicationChangeHistoryAdmin)
admin.site.register(OrganizerStatusSets, OrganizerStatusSetsAdmin)
