from rest_framework import serializers
from main.models import *


class OrganizerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organizer
        fields = '__all__'


class OrganizerEmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrganizerEmployee
        fields = '__all__'


class ApplicationSerializer(serializers.ModelSerializer):
    status_name = serializers.StringRelatedField(many=False)

    class Meta:
        model = Application
        fields = (
            'id',
            'is_being_edited',
            'editor',
            'author',
            'status',
            'status_name',
            'created_date',
            'delivery_date')

        extra_kwargs = {
            'author': {'write_only': True},
        }

    def to_internal_value(self, data):
        author = OrganizerAgent.objects.get(user_id=self.context['request'].user.id)
        data['delivery_date'] = '2021-11-11 06:00'
        data['author'] = author.id
        return super().to_internal_value(data)


class OrganizerStatusSetsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrganizerStatusSets
        fields = ('id', 'name',)
