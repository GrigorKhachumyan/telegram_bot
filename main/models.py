from django.contrib.auth.models import User
from django.db import models

from main.model_mixins import ApplicationBaseMixin
from shared.model_mixins import HistoryMixin


class Organizer(models.Model):
    name = models.CharField(unique=True, max_length=256, verbose_name='имя')

    class Meta:
        managed = True
        db_table = "organizer"
        verbose_name = "компания заказчика"
        verbose_name_plural = "компании заказчика"

    def __str__(self):
        return self.name


class Executor(models.Model):
    name = models.CharField(unique=True, max_length=256, verbose_name='имя')
    inn = models.CharField(unique=True, max_length=11, verbose_name='инн')

    class Meta:
        managed = True
        db_table = "executor"
        verbose_name = "компания перевозчиков"
        verbose_name_plural = "компании перевозчиков"

    def __str__(self):
        return self.name


class NativeExecutor(models.Model):
    host_company = models.ForeignKey("Organizer", models.CASCADE, related_name="native_executor", verbose_name='кампания заказчиков')
    fixed_price_per_km = models.FloatField(verbose_name='цена')
    contract = models.URLField(verbose_name='контракт')
    executor = models.OneToOneField("Executor", models.CASCADE, verbose_name='компания перевозчиков')

    class Meta:
        managed = True
        db_table = "native_executor"
        verbose_name = "перевозчик компании заказчиков"
        verbose_name_plural = "перевозчики компании заказчиков"


class OrganizerEmployee(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="agent", verbose_name='пользователь')
    company = models.ForeignKey(Organizer, on_delete=models.CASCADE, verbose_name='компания заказчика')

    class Meta:
        managed = True
        db_table = "organizer_agent"
        verbose_name = "Агент заказчика"
        verbose_name_plural = "Агенты заказчика"

    def __str__(self):
        return self.user.username


class ExecutorAgent(models.Model):
    name = models.CharField(max_length=45, verbose_name='имя')
    surname = models.CharField(max_length=45, verbose_name='фамилия')
    executor = models.ForeignKey('Executor', models.CASCADE, related_name="executor_agent", verbose_name='компания перевозчиков')

    class Meta:
        managed = True
        db_table = "executor_agent"
        verbose_name = "перевозчик"
        verbose_name_plural = "перевозчики"


class Application(ApplicationBaseMixin):
    is_being_edited = models.DateField(blank=True, null=True, verbose_name='время редактирования')
    editor = models.ForeignKey('OrganizerEmployee', blank=True, null=True, on_delete=models.CASCADE, related_name="editor_agent", verbose_name='редактор')

    class Meta:
        managed = True
        db_table = 'application'
        verbose_name = "заявка на перевозку"
        verbose_name_plural = "заявки на перевозки"


class ApplicationChangeHistory(models.Model, HistoryMixin):
    application = models.ForeignKey(Application, on_delete=models.CASCADE, verbose_name='заявка')

    class Meta:
        managed = True
        db_table = 'application-change-history'
        verbose_name = "история заявки"
        verbose_name_plural = "история заявок"


class DeliveryDirection(models.Model):
    country = models.CharField(max_length=128, verbose_name='страна')
    city = models.CharField(max_length=128, verbose_name='город')
    address = models.TextField(verbose_name='адрес')

    class Meta:
        managed = True
        db_table = 'delivery_direction'
        verbose_name = "адрес доставки"
        verbose_name_plural = "адреса доставок"


class ApplicationExecutorView(models.Model):
    executor = models.ForeignKey("ExecutorAgent", models.CASCADE, related_name="executor_seen", verbose_name='перевозчик')
    application = models.ForeignKey("Application", models.CASCADE, related_name="views", verbose_name='заявка')
    seen_date = models.DateField(blank=True, null=True, verbose_name='время просмотра')

    class Meta:
        managed = True
        db_table = 'application_executor_view'
        verbose_name = "посмотрели заявка"
        verbose_name_plural = "посмотрели заявки"


class ApplicationOffer(models.Model):
    executor = models.ForeignKey("ExecutorAgent", models.CASCADE, related_name="executor_offer", verbose_name='перевозчик')
    application = models.ForeignKey("Application", models.CASCADE, related_name="application_offer", verbose_name='заявка')
    offer_date = models.DateField(blank=True, null=True, verbose_name='время заявки')
    price = models.FloatField(verbose_name='цена')

    class Meta:
        managed = True
        db_table = 'application_executor_offer'
        verbose_name = "отклик перевозчиков"
        verbose_name_plural = "отклики перевозчиков"


class ApplicationComment(models.Model):
    executor = models.ForeignKey("ExecutorAgent", models.CASCADE, related_name="executor_comment", verbose_name='перевозчик')
    application = models.ForeignKey("Application", models.CASCADE, related_name="application_comment", verbose_name='заявка')
    comment_date = models.DateField(blank=True, null=True, verbose_name='время комментарии')
    comment = models.TextField(verbose_name='комментария')

    class Meta:
        managed = True
        db_table = 'application_executor_comment'
        verbose_name = "комментария заявки"
        verbose_name_plural = "комментарии заявок"


class OrganizerStatusSets(models.Model):
    name = models.CharField(max_length=256, verbose_name='имя статуса')
    company = models.ForeignKey(Organizer, on_delete=models.CASCADE, verbose_name='компания заказчика')

    class Meta:
        managed = True
        unique_together = ('name', 'company',)
        db_table = 'organizer_application_sets'
        verbose_name = "статус заявки"
        verbose_name_plural = "статусы заявок"

    def __str__(self):
        return self.name
