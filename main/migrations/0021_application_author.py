# Generated by Django 2.2.12 on 2020-12-30 12:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0020_auto_20201230_1142'),
    ]

    operations = [
        migrations.AddField(
            model_name='application',
            name='author',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='main.OrganizerAgent'),
            preserve_default=False,
        ),
    ]
