from rest_framework import (generics,
                            )

from main.pagination import CursorPagination
from main.serializers import *


class OrganizerAPIView(generics.ListCreateAPIView, generics.RetrieveUpdateDestroyAPIView):
    queryset = Organizer.objects.all()
    serializer_class = OrganizerSerializer


class OrganizerEmployeeAPIView(generics.ListCreateAPIView, generics.RetrieveUpdateDestroyAPIView):
    queryset = OrganizerEmployee.objects.all()
    serializer_class = OrganizerEmployeeSerializer


class ApplicationView(generics.ListAPIView,
                      generics.CreateAPIView,
                      generics.DestroyAPIView,
                      generics.GenericAPIView):
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer
    pagination_class = CursorPagination

    def get_queryset(self):
        qs = self.queryset.filter(author__user_id=self.request.user.id)
        return qs


class ApplicationStatusView(generics.ListAPIView, generics.GenericAPIView):
    queryset = OrganizerStatusSets.objects.all()
    serializer_class = OrganizerStatusSetsSerializer
