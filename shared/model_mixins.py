from django.contrib.auth.models import User
from django.db import models


class HistoryMixin:
    from_datetime = models.DateTimeField()
    to_datetime = models.DateTimeField()
    editor = models.ForeignKey(User, on_delete=models.PROTECT)
